<!doctype html>

<html <?php language_attributes(); ?> class="no-js">

<head>
  <meta charset="utf-8">
  <title><?php wp_title( '|', true, 'right' ); ?></title>
  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<header class="header" id="header">
  <div class="wrap hpad flex flex--center flex--justify">

    <?php 
      //logo SEO markup
      $site_name = get_bloginfo( 'name' );
      $logo_markup  = ( is_front_page() ) ? '<h2 class="visuallyhidden">' . $site_name . '</h2>' : '<p class="visuallyhidden">' . $site_name . '</p>';
     ?>

    <a class="header__logo" href="<?php bloginfo('url'); ?>">
      <?php echo $logo_markup; ?>
    </a>

    <div class="nav-toggle"> 
      <span class="nav-toggle__label"><?php _e('Menu', 'lionlab') ?></span> 
      <span class="nav-toggle__icon"></span>
    </div>

    <nav class="nav" role="navigation">
      <div class="nav--mobile">
        <?php scratch_main_nav(); ?>
      </div>
    </nav>

  </div>
</header>
