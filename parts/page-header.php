<?php 
	if ( is_tax() ) {
		$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
		$title = $term->name;
	} elseif ( is_category() ) {
		$title = single_cat_title( '', false );
	} elseif ( is_archive() ) {
		$title = post_type_archive_title( '', false );
	} else {
		$id = (is_home()) ? get_option('page_for_posts') : $post->ID;
		$title = get_proper_title($id);
	}

	//bg
	$page_img = get_field('page_img') ? : $page_img = get_field('page_img', 'options');	

	//badge text
	$badge = get_field('page_badge');

	//arc color
	$arc = get_field('arc_color');

	//figure
	$figure = get_field('figure');

	//products
	$shots = get_field('shots_product');
	$bags = get_field('bags_product');	

?>

<?php if (!is_front_page() ) : ?>
<section class="page__hero arc--<?php echo esc_attr($arc); ?>" style="background-image: url(<?php echo esc_url(($page_img['url'])); ?>);">
<?php else: ?>
<section class="page__hero page__hero--frontpage arc--<?php echo esc_attr($arc); ?>" style="background-image: url(<?php echo esc_url(($page_img['url'])); ?>);">
<?php endif; ?>
	<div class="wrap hpad page__container">
		<div class="page__desc">
			<h1 class="page__title"><?php echo $title; ?></h1>
			<?php if ($badge) : ?>
			<div class="page__badge"><?php echo $badge; ?></div>
			<?php endif; ?>
		</div>

		<?php if ($figure && is_front_page() ) : ?>
			<img class="page__figure--frontpage" src="<?php echo esc_url($figure['url']); ?>" alt="sismofytter_orginal">
		<?php endif; ?>

		<?php if (is_front_page() ) : ?>
		<div class="flex flex--wrap page__flex">
		<?php if ($shots) : ?>
			<div class="col-sm-4">
				<img class="page__figure--shots" src="<?php echo esc_url($shots['url']); ?>" alt="sismofytter_shots">
			</div>
		<?php endif; ?>

		<?php if ($bags) : ?>
		<div class="col-sm-8">
			<img class="page__figure--bags" src="<?php echo esc_url($bags['url']); ?>" alt="sismofytter_poser">
		</div>
		<?php endif; ?>
		</div>
		<?php endif; ?>

	</div>

	<?php if ($figure && !is_front_page() ) : ?>
		<img class="page__figure" src="<?php echo esc_url($figure['url']); ?>" alt="sismofytter_orginal">
	<?php endif; ?>



</section>